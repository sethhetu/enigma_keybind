This project is a simple key-binding example for the ENIGMA game engine. 
To use it in your game, you'll have to do two things:
  1) Move the user to rm_0 when you want to set key-bindings.
  2) Put obj_keytrack in any other room where you want to read input from the bound keys.

(In case you are unfamiliar with ENIGMA, you'll want to zip every file in this directory into a file called key_bind.egm,
 and then open that in ENIGMA).

The obj_keytrack Object will update key status in BeginStep (so you probably only want to check them in NormalStep). 
Key state can be accessed via global variable:
  global.KeyPressAction
  global.KeyPressCancel
  global.KeyPressLeft
  global.KeyPressUp
  global.KeyPressRight
  global.KeyPressDown

If you need the very first key press, you can check:
  global.KeyFirstPressAction

...etc. 

This project is coded somewhat messily and could definitely use some cleanup, but overall it works fine. 
You should be able to extend it easily enough, adding new keys, virtual key bindings, etc.

I am releasing the source code to this project into the Public Domain.
~Seth


